cd C:\xampp\mysql\bin
mysql -uroot

------------------------
Soal 1 Membuat Database 
------------------------
create database myshop;


---------------------------------------
Soal 2 Membuat Table di Dalam Database 
---------------------------------------
1. create table users(
    -> id int(8) auto_increment,
    -> name varchar(255),
    -> email varchar(255),
    -> password varchar(255),
    -> primary key(id)
    -> );

2.  create table items(
    -> id int(8) auto_increment,
    -> name varchar(255),
    -> description varchar(255),
    -> price int,
    -> stock int,
    -> category_id int,
    -> primary key(id),
    -> foreign key(category_id) references categories(id)
    -> );

3. create table categories(
    -> id int(8) auto_increment,
    -> name varchar(255),
    -> primary key(id)
    -> );


----------------------------------
Soal 3 Memasukkan Data pada Table 
----------------------------------
1. Users
insert into users(name, email, password)
    -> values ("John Doe", "john@doe.com", "john123");
insert into users(name, email, password)
    -> values ("Jane Doe", "jane@doe.com", "jenita123");

2. Categories
 insert into categories(name) values("gadget"), ("cloth"), ("men"), ("women"), ("branded");

3. Items 
insert into items(name, description, price, stock) values("Sumsang b50", "hape keren dari merek sumsang", 4000000, 100), ("Uniklooh", "baju keren dari brand ternama", 500000, 50), ("IMH0 Watch", "jam tangan anak yang jujur banget", 2000000, 10);
update items set category_id = 1 where id=30;
update items set category_id = 2 where id=31;
update items set category_id = 1 where id=32;


------------------------------------
Soal 4 Mengambil Data dari Database 
------------------------------------
a. select*from users without password;
 select id, name, email from users;

b. select price from items where price > 1000000;
select*from items where name like 'sum%';

c. select items.name, items.description, items.price, items.stock, items.category_id, categories.name as kategori from items inner join categories on items.category_id = categories.id;

-----------------------------------
Soal 5 Mengubah Data dari Database 
-----------------------------------
update items set price=2500000 where price= 4000000;